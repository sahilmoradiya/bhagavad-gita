//
//  chapterCell.swift
//  Bhagavad Gita
//
//  Created by Sahil Moradiya on 31/08/21.
//

import UIKit

class chapterCell: UITableViewCell {
    
    @IBOutlet var lblName: UILabel!
    @IBOutlet var btnChapter: UIButton!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
