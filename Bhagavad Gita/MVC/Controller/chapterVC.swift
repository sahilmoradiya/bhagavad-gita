//
//  chapterVC.swift
//  Bhagavad Gita
//
//  Created by Sahil Moradiya on 31/08/21.
//

import UIKit
import Alamofire

class chapterVC: UIViewController,UITableViewDelegate,UITableViewDataSource {

    //MARK:- IBOutlet
    
    @IBOutlet var viewWorking: UIView!
    @IBOutlet var lblError: UILabel!
    @IBOutlet var tblView: UITableView!
    
    var arrChapter:[ChapterRootClass] = [ChapterRootClass]()
    
    //MARK:- View Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()

        APIClient.sharedInstance.networkconnect()
        viewWorking.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        lblError.isHidden = true
        tblView.isHidden = false
        tblView.delegate = self
        tblView.dataSource = self
        chapterApi()
    }
    
    //MARK: - TableView Delegate & DataSource
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrChapter.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tblView.dequeueReusableCell(withIdentifier: "chapterCell") as! chapterCell
        
        cell.btnChapter.tag = indexPath.row
        
        let dicData = arrChapter[indexPath.row]
        let ch = dicData.chapterNumber
        cell.lblName.text = "Chapter \(ch ?? 0)"
        cell.btnChapter.addTarget(self, action: #selector(clickChapter(sender:)), for: .touchUpInside)
        return cell
    }
    
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 62
    }
    
    @objc func clickChapter(sender: UIButton)
    {
        print(sender.tag)
        let chdetailVC:chdetailVC = self.storyboard?.instantiateViewController(withIdentifier: "chdetailVC") as! chdetailVC
        chdetailVC.dicChapterRootClass = arrChapter[sender.tag]
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.pushViewController(chdetailVC, animated: true)
    }

    //MARK:- API Calling
    
    func chapterApi()
    {
        APIClient.sharedInstance.showIndicator()
            
        APIClient.sharedInstance.MakeAPICallWithAuthHeaderGetArray(chapters, parameters: [:]) { response, error, statusCode  in
            
            print("Response : \(String(describing: response))")
            print("Status Code : \(String(describing: statusCode))")
            
            APIClient.sharedInstance.hideIndicator()
            
            if error == nil
            {
                let arrData = response
                
                for obj in arrData!
                {
                    let dicData = ChapterRootClass(fromDictionary: (obj as? NSDictionary)!)
                    self.arrChapter.append(dicData)
                }
                self.tblView.reloadData()
            }
            else if response == nil
            {
                self.tblView.isHidden = true
                self.lblError.isHidden = false
            }
            else
            {
                self.view.makeToast("\(String(describing: error))")
            }
        }
    }
}

