//
//  chdetailVC.swift
//  Bhagavad Gita
//
//  Created by Sahil Moradiya on 31/08/21.
//

import UIKit
import AVFoundation

class chdetailVC: UIViewController, AVSpeechSynthesizerDelegate {

    //MARK:- IBOutlet
    
    @IBOutlet var btnLangauge: UIButton!
    @IBOutlet var lblSummary: UITextView!
    @IBOutlet var btnplay: UIButton!
    @IBOutlet var btnstop: UIButton!
    @IBOutlet var btnPause: UIButton!
    @IBOutlet var lblChapter: UILabel!
    @IBOutlet var mainView: UIView!
    @IBOutlet var lblTitle: UILabel!
    @IBOutlet var lblMeaning: UITextView!

    var dicChapterRootClass: ChapterRootClass?
    var Hindi:Bool = true
    var pause:Bool = true
    let synthesizer = AVSpeechSynthesizer()
    
    //MARK:- View Cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setUpHindi()
        mainView.layer.maskedCorners = [.layerMinXMinYCorner,.layerMaxXMinYCorner]
        btnplay.isHidden = false
        btnstop.isHidden = true
        btnPause.isHidden = true
        btnPause.setImage(UIImage(systemName: "pause.fill"), for: .normal)
        synthesizer.delegate = self
    }
    
    
    //MARK:- Functions
    
    func setUpEnglish()
    {
        Hindi = false
        self.lblChapter.text = "Chapter \(dicChapterRootClass?.chapterNumber ?? 0)"
        self.lblTitle.text = dicChapterRootClass?.transliteration
        self.lblMeaning.text = dicChapterRootClass?.meaning.en
        self.lblSummary.text = dicChapterRootClass?.summary.en
    }
    
    func speechSynthesizer(_ synthesizer: AVSpeechSynthesizer,
                      didFinish utterance: AVSpeechUtterance) {
        // Implements here.
        btnplay.isHidden = false
        btnstop.isHidden = true
        btnPause.isHidden = true
    }
    
    func setUpHindi()
    {
        Hindi = true
        self.lblChapter.text = "Chapter \(dicChapterRootClass?.chapterNumber ?? 0)"
        self.lblTitle.text = dicChapterRootClass?.name
        self.lblMeaning.text = dicChapterRootClass?.meaning.hi
        self.lblSummary.text = dicChapterRootClass?.summary.hi
    }
    
    func speechStart()
    {
        let utterance = AVSpeechUtterance(string: "\(self.lblTitle.text ?? "") Meaning  \(self.lblMeaning.text ?? "") Summary \(self.lblSummary.text ?? "")")
        if Hindi
        {
            utterance.voice = AVSpeechSynthesisVoice(language: "hi")
            utterance.rate = 0.4
        }
        else
        {
            utterance.voice = AVSpeechSynthesisVoice(language: "en-US")
            utterance.rate = 0.4
        }
        synthesizer.speak(utterance)
    }
    
    func speechStop()
    {
        synthesizer.stopSpeaking(at: AVSpeechBoundary.immediate)
    }
    
    func speechPause()
    {
        if synthesizer.isSpeaking
        {
            synthesizer.pauseSpeaking(at: AVSpeechBoundary.immediate)
        }
    }

    func speechContinue()
    {
        if synthesizer.isPaused
        {
            synthesizer.continueSpeaking()
        }
    }
    //MARK:- IBActions
    
    @IBAction func TapOnBack(_ sender: UIButton) {
        speechStop()
        self.navigationController?.popViewController(animated: true)
    }
    @IBAction func TapOnLangauge(_ sender: UIButton) {
        
        if Hindi
        {
            btnLangauge.setTitle("HN", for: .normal)
            btnplay.isHidden = false
            btnstop.isHidden = true
            btnPause.isHidden = true
            speechStop()
            setUpEnglish()
        }
        else
        {
            btnLangauge.setTitle("EN", for: .normal)
            btnplay.isHidden = false
            btnstop.isHidden = true
            btnPause.isHidden = true
            speechStop()
            setUpHindi()
        }
    }
    
    
    @IBAction func TapOnPlay(_ sender: UIButton) {
        
        btnstop.isHidden = false
        btnplay.isHidden = true
        btnPause.isHidden = false
        btnPause.setImage(UIImage(systemName: "pause.fill"), for: .normal)
        speechStart()
    }
    
    @IBAction func TapOnStop(_ sender: UIButton) {
        
        btnplay.isHidden = false
        btnstop.isHidden = true
        btnPause.isHidden = true
        btnPause.setImage(UIImage(systemName: "pause.fill"), for: .normal)
        speechStop()
    }
    
    
    @IBAction func TapOnPause(_ sender: UIButton) {
        
        if pause
        {
            btnPause.setImage(UIImage(systemName: "play.fill"), for: .normal)
            speechPause()
            pause = false
        }
        else
        {
            btnPause.setImage(UIImage(systemName: "pause.fill"), for: .normal)
            speechContinue()
            pause = true
        }
    }
}
