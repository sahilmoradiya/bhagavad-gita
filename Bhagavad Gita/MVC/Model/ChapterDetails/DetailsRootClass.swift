//
//	DetailsRootClass.swift
//
//	Create by Sahil Moradiya on 31/8/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class DetailsRootClass : NSObject, NSCoding{

	var chapterNumber : Int!
	var meaning : DetailsMeaning!
	var name : String!
	var summary : DetailsMeaning!
	var translation : String!
	var transliteration : String!
	var versesCount : Int!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		chapterNumber = dictionary["chapter_number"] as? Int == nil ? 0 : dictionary["chapter_number"] as? Int
		if let meaningData = dictionary["meaning"] as? NSDictionary{
			meaning = DetailsMeaning(fromDictionary: meaningData)
		}
		else
		{
			meaning = DetailsMeaning(fromDictionary: NSDictionary.init())
		}
		name = dictionary["name"] as? String == nil ? "" : dictionary["name"] as? String
		if let summaryData = dictionary["summary"] as? NSDictionary{
			summary = DetailsMeaning(fromDictionary: summaryData)
		}
		else
		{
			summary = DetailsMeaning(fromDictionary: NSDictionary.init())
		}
		translation = dictionary["translation"] as? String == nil ? "" : dictionary["translation"] as? String
		transliteration = dictionary["transliteration"] as? String == nil ? "" : dictionary["transliteration"] as? String
		versesCount = dictionary["verses_count"] as? Int == nil ? 0 : dictionary["verses_count"] as? Int
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if chapterNumber != nil{
			dictionary["chapter_number"] = chapterNumber
		}
		if meaning != nil{
			dictionary["meaning"] = meaning.toDictionary()
		}
		if name != nil{
			dictionary["name"] = name
		}
		if summary != nil{
			dictionary["summary"] = summary.toDictionary()
		}
		if translation != nil{
			dictionary["translation"] = translation
		}
		if transliteration != nil{
			dictionary["transliteration"] = transliteration
		}
		if versesCount != nil{
			dictionary["verses_count"] = versesCount
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         chapterNumber = aDecoder.decodeObject(forKey: "chapter_number") as? Int
         meaning = aDecoder.decodeObject(forKey: "meaning") as? DetailsMeaning
         name = aDecoder.decodeObject(forKey: "name") as? String
         summary = aDecoder.decodeObject(forKey: "summary") as? DetailsMeaning
         translation = aDecoder.decodeObject(forKey: "translation") as? String
         transliteration = aDecoder.decodeObject(forKey: "transliteration") as? String
         versesCount = aDecoder.decodeObject(forKey: "verses_count") as? Int

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if chapterNumber != nil{
			aCoder.encode(chapterNumber, forKey: "chapter_number")
		}
		if meaning != nil{
			aCoder.encode(meaning, forKey: "meaning")
		}
		if name != nil{
			aCoder.encode(name, forKey: "name")
		}
		if summary != nil{
			aCoder.encode(summary, forKey: "summary")
		}
		if translation != nil{
			aCoder.encode(translation, forKey: "translation")
		}
		if transliteration != nil{
			aCoder.encode(transliteration, forKey: "transliteration")
		}
		if versesCount != nil{
			aCoder.encode(versesCount, forKey: "verses_count")
		}

	}

}