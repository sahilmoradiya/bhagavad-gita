//
//	DetailsMeaning.swift
//
//	Create by Sahil Moradiya on 31/8/2021
//	Copyright © 2021. All rights reserved.
//	Model file generated using JSONExport: https://github.com/Ahmed-Ali/JSONExport

import Foundation


class DetailsMeaning : NSObject, NSCoding{

	var en : String!
	var hi : String!


	/**
	 * Overiding init method
	 */
	init(fromDictionary dictionary: NSDictionary)
	{
		super.init()
		parseJSONData(fromDictionary: dictionary)
	}

	/**
	 * Overiding init method
	 */
	override init(){
	}

	/**
	 * Instantiate the instance using the passed dictionary values to set the properties values
	 */
	@objc func parseJSONData(fromDictionary dictionary: NSDictionary)
	{
		en = dictionary["en"] as? String == nil ? "" : dictionary["en"] as? String
		hi = dictionary["hi"] as? String == nil ? "" : dictionary["hi"] as? String
	}

	/**
	 * Returns all the available property values in the form of NSDictionary object where the key is the approperiate json key and the value is the value of the corresponding property
	 */
	func toDictionary() -> NSDictionary
	{
		let dictionary = NSMutableDictionary()
		if en != nil{
			dictionary["en"] = en
		}
		if hi != nil{
			dictionary["hi"] = hi
		}
		return dictionary
	}

    /**
    * NSCoding required initializer.
    * Fills the data from the passed decoder
    */
    @objc required init(coder aDecoder: NSCoder)
	{
         en = aDecoder.decodeObject(forKey: "en") as? String
         hi = aDecoder.decodeObject(forKey: "hi") as? String

	}

    /**
    * NSCoding required method.
    * Encodes mode properties into the decoder
    */
    public func encode(with aCoder: NSCoder) 
	{
		if en != nil{
			aCoder.encode(en, forKey: "en")
		}
		if hi != nil{
			aCoder.encode(hi, forKey: "hi")
		}

	}

}