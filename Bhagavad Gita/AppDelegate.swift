//
//  AppDelegate.swift
//  Bhagavad Gita
//
//  Created by Sahil Moradiya on 31/08/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        return true
    }

    func home()
    {
        let storyboard = UIStoryboard.init(name: "Main", bundle: nil)
        let vc = storyboard.instantiateViewController(withIdentifier: "chdetailVC") as! chdetailVC
        let navigation = UINavigationController.init(rootViewController: vc)
        navigation.navigationBar.isTranslucent = true
        self.window?.rootViewController = navigation
    }

}

